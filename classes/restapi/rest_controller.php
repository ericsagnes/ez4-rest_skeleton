<?php

class myRestController extends ezcMvcController{

  protected $body_parameters = array();
  protected $post_parameters = array();
  protected $request_files = array();

  public function __get( $name ){   
    if ( isset( $this->body_parameters[$name] ) ){   
      return $this->body_parameters[$name];
    }
    $_name = str_replace( '_', '-', $name );
    if ( isset( $this->body_parameters[$_name] ) ){   
      return $this->body_parameters[$_name];
    }
    if ( isset( $this->post_parameters[$name] ) ){   
      return $this->post_parameters[$name];
    }
    return parent::__get( $name );
  }   

  public function __isset( $name ){   
    if( array_key_exists( $name, $this->body_parameters ) ){
      return true;
    }
    $_name = str_replace( '_', '-', $name );
    if( array_key_exists( $_name, $this->body_parameters ) ){
      return true;
    }
    if( array_key_exists( $name, $this->post_parameters ) ){
      return true;
    }
    return parent::__isset( $name );
  } 

  // post & post application/json suport
  protected function setRequestVariables( ezcMvcRequest $request ){   
    parent::setRequestVariables( $request );
    $request = $this->request;
    if( $request->protocol == "http-post" && $request->raw['CONTENT_TYPE'] == "application/json" ){
      $this->body_parameters = json_decode( $request->body, true );
    }
    if( ! empty( $request->post ) ){
      $this->post_parameters = $request->post;
    }
    $this->request_files = $request->files;
  } 

  // Controller Logic
  public function doSomething(){

    $result = new ezcMvcResult();

    // ... Do some logic ..

    $status = true;

    if( $status ){
      $result->variables['_status_code'] = 201;
      $result->variables['status'] = 'success';
    } else {
      $result->variables['_status_code'] = 400;
      $result->variables['status'] = 'failed';
      $result->variables['error_message'] = 'ooops..';
    }

    return $result;
  }

}

?>
