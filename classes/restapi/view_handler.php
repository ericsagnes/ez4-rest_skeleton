<?php

class myViewHandler implements ezcMvcViewHandler{

  protected $zoneName;
  protected $result;

  protected $variables = array();

  public function __construct( $zoneName, $templateLocation = null ){
    $this->zoneName = $zoneName;
  }

  public function send( $name, $value ){
    $this->variables[$name] = $value;
  }

  public function process( $last ){
    $this->result = $this->variables;
    // _file key is for direct sending files
    if( array_key_exists( '_file', $this->result ) && $last ){
      $this->result = file_get_contents( $this->result['_file'] ); 
    } else if ( $last ){
      $this->result = json_encode( $this->result );
    }
  }

  public function __get( $name ){
    return $this->variables[$name];
  }

  public function __isset( $name ){
    return array_key_exists( $name, $this->variables );
  }

  public function getName(){
    return $this->zoneName;
  }

  public function getResult(){
    return $this->result;
  }

}

?>
