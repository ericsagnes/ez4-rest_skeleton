<?php

class myRestApiProvider implements ezpRestProviderInterface
{

  // Defining routes
  public function getRoutes(){
    $routes = array(
      'somthing' => new ezpRestVersionedRoute(
        new ezpMvcRailsRoute(
          '/foo/something', 'myRestController',
          array( 'http-post' => 'something' )
        ), 1
      )
    );
    return $routes;
  }

  public function getViewController(){
    return new myRestApiViewController();
  }

}

?>
