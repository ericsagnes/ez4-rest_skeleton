<?php

class MyAuthFilter extends ezcAuthenticationFilter{
  const STATUS_INVALID_USER = 1;

  public function __construct( $user_data ){
    $this->user_data = $user_data;
  }

  // Logic for authentication
  public function run( $credentials )
  {   
    $status = self::STATUS_INVALID_USER;
    if( is_array($this->user_data) ){
        $status = self::STATUS_OK;
      }
    }

    return $status;
  }   
}

?>
