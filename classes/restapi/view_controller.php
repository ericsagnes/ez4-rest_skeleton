<?php

class myRestApiViewController implements ezpRestViewControllerInterface{

  public function loadView( ezcMvcRoutingInformation $routeInfo, ezcMvcRequest $request, ezcMvcResult $result ){
    return new myRestView( $request, $result );
  }

}

?>
