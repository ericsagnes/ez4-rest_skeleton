<?php

class myRestView extends ezcMvcView{

  public function __construct( ezcMvcRequest $request, ezcMvcResult $result ){   
    parent::__construct( $request, $result );
    // adding custom status code support
    if( array_key_exists( '_status_code', $result->variables ) ){
      $result->status = new ezpRestStatusResponse(
        $result->variables['_status_code']
      ); 
      unset( $result->variables['_status_code'] );
    }
    $result->content = new ezcMvcResultContent();
    // file sending support with the _file variable
    if( array_key_exists( '_file', $result->variables ) ){
      $result->content->type = "application/octet-stream";
      $result->content->encoding = "binary";
      $result->content->disposition = new ezcMvcResultContentDisposition( 'attachment', $result->variables['_file_name'] );
    } else {
      $result->content->type = "application/json";
      $result->content->charset = "UTF-8";
    }
  }   

  public function createZones( $layout ){   
    $zones = array();
    $zones[] = new myViewHandler( 'content' );
    return $zones;
  }

}

?>
