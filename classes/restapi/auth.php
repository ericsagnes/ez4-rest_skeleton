<?php

class ezpMyAuthStyle extends ezpRestAuthenticationStyle implements ezpRestAuthenticationStyleInterface
{
  public function setup( ezcMvcRequest $request ){

    $user_id = false;
    $user_data = false;

    $body_parameters = array();
    if( $request->protocol == "http-post" && $request->raw['CONTENT_TYPE'] == "application/json" ){
      $body_parameters = json_decode( $request->body, true );
    }

    // ... Auth Logic ...

    $cred = new ezcAuthenticationIdCredentials( $user_id );
    $auth = new ezcAuthentication( $cred );
    $auth->addFilter( new MyAuthFilter( $user_data ) );
    return $auth;
  }   

  public function authenticate( ezcAuthentication $auth, ezcMvcRequest $request ){   
    if ( !$auth->run() && $request->uri !== "{$this->prefix}/fatal" ){
      throw new ezpUserNotFoundException( $auth->credentials->id );
    }else{   
      return eZUser::fetch( $auth->credentials->id );
    }
  }

}

?>
